// import { Card } from 'components'
import { useEffect } from "react"
import { useSelector } from "react-redux"
import { RootState, useAppDispatch } from "store"
import { lichChieuThunk } from "store/lichChieu"
import { CumRap, Tabs } from "."

export const BannerLichChieu = () => {
    const dispatch = useAppDispatch()
    const { lstCumRap } = useSelector((state: RootState) => state.lichChieu)
    console.log("lstCumRap: ", lstCumRap);

    useEffect(() => {
        dispatch(lichChieuThunk())
    }, [dispatch])

    // const onChange = (activeKey: string) => {
    //     setSearchParams({
    //       movieTheater: activeKey
    //     })
    //   };
    return (
        <div>
            <Tabs
          tabPosition="left"
          onChange={(activeKey) => {
            console.log(activeKey)
          }}
        //   onChange={onChange}
          items={lstCumRap?.map((list) => {
            return {
              key: String(list.maHeThongRap),
              label: list.tenHeThongRap,
              children: <CumRap cumRap={list.lstCumRap}/>,
            }
          })}
        />
            {
                lstCumRap?.map((list) => {
                    return (
                        <div key={list.maHeThongRap} className="grid grid-cols-12">
                            <div className="col-start-1 col-span-1">
                                <img src={list.logo} alt="" width="100px" />
                                <span>{list.tenHeThongRap}</span>
                            </div>
                            <p className="col-start-2 col-span-11">{list.lstCumRap.map((cumRap) => {
                                return (
                                    <div key={cumRap.maCumRap} className="grid grid-cols-12">
                                        <div className="col-start-1 col-span-2">
                                            <img src={cumRap.hinhAnh} alt="" width="100px" />
                                            <p>{cumRap.tenCumRap}</p>
                                            <p>{cumRap.diaChi}</p>
                                        </div>
                                        <p className="col-start-3 col-span-10">{cumRap.danhSachPhim.map((flim) => {
                                            return (
                                                <div key={flim.maPhim} className="grid grid-cols-6">
                                                    <div className="col-start-1 col-span-2">
                                                        <img src={flim.hinhAnh} alt="" width="200px" height="200px" />
                                                        <span>{flim.tenPhim}</span>
                                                    </div>
                                                    <p className="col-start-3 col-span-4">{flim.lstLichChieuTheoPhim.map((filmInfo) => {
                                                        return (
                                                            <span key={filmInfo.maLichChieu}>{filmInfo.ngayChieuGioChieu}</span>
                                                        )
                                                    })}</p>
                                                </div>
                                            )
                                        })}</p>
                                    </div>
                                )
                            })}</p>
                        </div>
                    )
                })
            }
        </div>
    )
}
