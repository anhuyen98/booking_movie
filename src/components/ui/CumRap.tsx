// import { CumRap, DanhSachPhim, LichChieuTheoPhim } from 'types'

type CumRapType = {
    cumRap?: []
}

export const CumRap = ( {cumRap} : CumRapType) => {
  return (
    <div>
        {
            cumRap?.map((cumRapNe) => {
                return (
                    <div>{cumRapNe.danhSachPhim}</div>
                )
            })
        }
    </div>
  )
}
