import { apiInstance } from "constant/apiInstance";
import { CumRap, DanhSachPhim, LichChieuTheoPhim, LstCumRap } from "types";

const api = apiInstance({
    baseURL: import.meta.env.VITE_THONG_TIN_RAP_API,
})

export const lichChieuServices = {   
    getDanhSachPhim: () => api.get<ApiResponse<LstCumRap<CumRap<DanhSachPhim<LichChieuTheoPhim>>>>>("/LayThongTinLichChieuHeThongRap?maNhom=GP13"),
}