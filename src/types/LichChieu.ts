export type LstCumRap<H> = {
    lstCumRap: H;
    maHeThongRap: string;
    tenHeThongRap: string;
    logo: string;
    mahom: string;
  }[];
  
  export type CumRap<H> = {
    danhSachPhim: H;
    maCumRap: string;
    tenCumRap: string;
    hinhAnh: string;
    diaChi: string;
  }[];
  
  export type DanhSachPhim<H> = {
    lstLichChieuTheoPhim: H;
    maPhim: number;
    tenPhim: string;
    hinhAnh: string;
    hot: boolean;
    dangChieu: boolean;
    sapChieu: boolean;
  }[];
  
  export type LichChieuTheoPhim = {
    maLichChieu: number;
    maRap: string;
    tenRap: string;
    ngayChieuGioChieu: string;
    giaVe: number;
  }[];




